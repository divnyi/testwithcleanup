# TestWithCleanup

[![Version](https://img.shields.io/cocoapods/v/TestWithCleanup.svg?style=flat)](https://cocoapods.org/pods/TestWithCleanup)
[![License](https://img.shields.io/cocoapods/l/TestWithCleanup.svg?style=flat)](https://cocoapods.org/pods/TestWithCleanup)
[![Platform](https://img.shields.io/cocoapods/p/TestWithCleanup.svg?style=flat)](https://cocoapods.org/pods/TestWithCleanup)

## What?

This is helper functions I made to make UI testing easier.

The killer feature is TestWithCleanup class, that will replace you XCTestCase class. It allows to reuse current screen state (after cleaning it up) to run next test case. If the test failed, it will re-initialize app state and run tests again. It will only fail a test on a clean run.

## Why?

If you have situations when getting to some specific screen is troublesome (long proccess), this class will save you a good amount of time.

## Also..

It contais some helper classes to make work with Xcode UI Tests easier.

