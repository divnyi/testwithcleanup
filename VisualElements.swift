import XCTest

/// umbrella class, create your string enums
/// (with conformance to IdentifierEnum and VisualScreen) under it
class VisualElements: NSObject {
    static var storage: [String: XCUIElement] = [:]
}

protocol VisualScreen {
    static func isCurrentScreen() -> Bool
}
