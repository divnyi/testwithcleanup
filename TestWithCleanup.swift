import XCTest

/// Use instead of XCTestCase, override restartInitialState(), cleanupState(), assertInitialState()
class TestWithCleanup: XCTestCase {
    func restartInitialState() {
        XCTFail("restartInitialState not implemented")
    }
    func cleanupState() {
        XCTFail("cleanupState not implemented")
    }
    func assertInitialState() {
        XCTFail("isInitialState not implemented")
    }

    let app = XCUIApplication()
    
    var cleanRun = false
    static var initializationIsBroken = false
    
    override class func setUp() {
        super.setUp()
        initializationIsBroken = false
    }
    
    override func setUp() {
        super.setUp()
        
        self.continueAfterFailure = false

        if cleanRun == false {
            self.cleanupState()
        }
        
        if cleanRun == true { TestWithCleanup.initializationIsBroken = true }
        self.assertInitialState()
        if cleanRun == true { TestWithCleanup.initializationIsBroken = false }
    }
    
    override func invokeTest() {
        if TestWithCleanup.initializationIsBroken {
            XCTFail("Initialization was failed in another test case -- failing fast")
            return
        }

        let didFail = tryBlock {
            self.setUp()
            self.invocation?.invoke()
        } != nil

        if didFail == false {
            XCUIUtils.takeScreenshot(named: "Success screenshot")
        }
        if didFail && self.cleanRun == false {
            self.cleanRun = true
            self.restartInitialState()
            self.invokeTest()
        } else {
            self.tearDown()
        }
    }
    
    override func recordFailure(withDescription description: String,
                                inFile filePath: String,
                                atLine lineNumber: Int,
                                expected: Bool) {
        if cleanRun == false && TestWithCleanup.initializationIsBroken == false {
            XCUIUtils.takeScreenshot(named: "Unclean run failure screenshot")
            print("\n***  \(description) in \(filePath):\(lineNumber), expected \"\(expected)\" ***\n***  Did fail on non-clean run, restarting ***\n")
            NSException(name: NSExceptionName("unclean_run_caught_error"), reason: "because", userInfo: [:]).raise()
        } else {
            XCUIUtils.takeScreenshot(named: "Clean run failure screenshot")
            super.recordFailure(withDescription: description,
                                inFile: filePath,
                                atLine: lineNumber,
                                expected: expected)
        }
    }
}
