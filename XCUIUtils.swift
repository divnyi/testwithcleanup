import XCTest

class XCUIUtils: NSObject {
    static func takeScreenshot(named: String) {
        XCTContext.runActivity(named: named) { (activity) in
            let screen = XCUIScreen.main
            let fullscreenshot = screen.screenshot()
            let fullScreenshotAttachment = XCTAttachment(screenshot: fullscreenshot)
            fullScreenshotAttachment.lifetime = .keepAlways
            activity.add(fullScreenshotAttachment)
        }
    }
}
