import XCTest
import Foundation

/// use string enum with identifiers to conform to this protocol
protocol IdentifierEnum: CaseIterable, RawRepresentable where Self.RawValue == String {
    var mustBeVisible: Bool { get }
}

extension IdentifierEnum {
    var identifier: String { return self.rawValue }
    var mustBeVisible: Bool { return true }
    
    private func setupElement() -> XCUIElement {
        let query = XCUIApplication().query(identifier: self.identifier)
        XCTAssert(query.count <= 1, "There is more than one element with identifier \(self.identifier)")
        let item = query.firstMatch
        VisualElements.storage[self.identifier] = item
        return item
    }
    func element() -> XCUIElement {
        if let element = VisualElements.storage[self.identifier] {
            return element
        } else {
            return setupElement()
        }
    }
    static func assertAllCasesExistAndVisible() {
        for item in self.allCases {
            if let error = item.element().checkIfAppIsRunning() {
                XCTFail("Element \(item.identifier): \(error)")
                return
            }
            if let error = item.element().checkIfExist() {
                XCTFail("Element \(item.identifier): \(error)")
                return
            }
            if item.mustBeVisible {
                if let error = item.element().checkIfVisible() {
                    XCTFail("Element \(item.identifier): \(error)")
                    return
                }
            }
        }
    }
}
