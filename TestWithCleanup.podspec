#
# Be sure to run `pod lib lint TestWithCleanup.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TestWithCleanup'
  s.version          = '0.1.0'
  s.summary          = 'XCode UI Testing with cleaning current screen state instead of relaunching app'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  The killer feature is TestWithCleanup class, that will replace you XCTestCase class. It allows to reuse current screen state (after cleaning it up) to run next test case. If the test failed, it will re-initialize app state and run tests again. It will only fail a test on a clean run.
                       DESC

  s.homepage         = 'https://gitlab.com/divnyi/testwithcleanup'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'WTFPL', :file => 'LICENSE' }
  s.author           = { 'Oleksii Horishnii' => 'oleksii.horishnii@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/divnyi/testwithcleanup.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'TestWithCleanup/Classes/**/*'
  
  # s.resource_bundles = {
  #   'TestWithCleanup' => ['TestWithCleanup/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
