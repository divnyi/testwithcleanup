import XCTest

extension XCUIElement {
    func query(identifier: String) -> XCUIElementQuery {
        return self.descendants(matching: .any).matching(identifier: identifier)
    }
    
    func checkIfExistAndVisible() -> String? {
        return checkIfAppIsRunning() ?? checkIfExist() ?? checkIfVisible()
    }
    
    func checkIfAppIsRunning() -> String? {
        if XCUIApplication().state != .runningForeground {
            return "app is not running"
        }
        return nil
    }
    
    func checkIfExist() -> String? {
        if self.exists == false {
            if self.waitForExistence(timeout: 1.0) == false {
                return "does not exist"
            }
        }
        return nil
    }
    
    func checkIfVisible() -> String? {
        if frame.isEmpty {
            return "frame is empty"
        }
        if !self.isHittable {
            return "is not hittable"
        }
        return nil
    }
    
    var textFieldValue: String? {
        return self.value as? String
    }
    
    func clearTextField() {
        let str = self.textFieldValue ?? ""
        if str.count > 0 {
            let deleteSeq = String(repeating: XCUIKeyboardKey.delete.rawValue, count: str.count)
            self.tap()
            self.typeText(deleteSeq)
        }
    }
    
    func tapAndType(_ text: String) {
        self.tap()
        self.typeText(text)
    }
}
